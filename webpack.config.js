/* eslint-disable max-len */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const outputDirectory = './dist';
module.exports = {
  // webpack will take the files from ./src/client/index
  entry: ['babel-polyfill', './src/client/index'],

  // and output it into /dist as bundle.js
  output: {
    path: path.join(__dirname, outputDirectory),
    filename: 'bundle.js'
  },

  // adding .ts and .tsx to resolve.extensions will help babel look for .ts and .tsx files to transpile
  resolve: {
    extensions: ['*', '.ts', '.tsx', '.js']
  },

  module: {
    rules: [

      // we use babel-loader to load our jsx and tsx files
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        },
      },

      // css-loader to bundle all the css files into one file and style-loader to add all the styles  inside the style tag of the document
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },

      // include images
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  devServer: {
    port: 3000,
    open: true,
    proxy: {
      '/api': 'http://localhost:8080'
    }
  },
  devtool: 'source-map',
  watch: true,
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './src/client/index.html'),
      favicon: path.join(__dirname, './src/client/public/favicon.ico')
    })
  ]
};
