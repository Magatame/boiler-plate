import * as express from 'express';
import * as core from "express-serve-static-core";
import IController from './baseController.interface';
import { User } from '../entity/User';
 
class UsersController implements IController {
  public path : string = '/users';
  public router : core.Router = express.Router();
 
  constructor() {
    this.intializeRoutes();
  }
 
  public intializeRoutes() {
    this.router.get(this.path, this.get);
    this.router.post(this.path, this.register);
  }
 
  get = (_request: express.Request, _response: express.Response) => {
    // TO DO
  }
 
  register = async (request: express.Request, response: express.Response) => {
    const user = new User();
    user.firstName = request.body.firstName;
    user.lastName = request.body.lastName;
    user.age = request.body.age;
    return user.save()
              .then(user => {
                return response.send(user)
              });
  }
}
 
export default UsersController;