import * as core from "express-serve-static-core";
 
interface IController {
    path : string;
    router : core.Router;
    intializeRoutes() : void;
}
   
export default IController;