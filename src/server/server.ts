import "reflect-metadata"
import App from './app';
import UsersController from './controllers/users.controller';
import { createConnection } from 'typeorm';

async function init() {
  const app = new App();
  const isUnitTests : boolean = process.env.NODE_ENV === 'test';
  const connections = new Map();

  connections.set('test', {
    "type": "sqlite",
    "database": ":memory:",
    "dropSchema": true,
    "synchronize": true,
    "logging": false,
    "entities": [__dirname + "/entity/*.ts"]
  });

  connections.set('development', {
    "synchronize": true,
    "type": "mysql",
    "host": "db",
    "port": 3306,
    "username": "am-i",
    "password": "am-i",
    "database": "am-i",
    "entities": [__dirname + "/entity/*.ts"]   
  })

  await app.initialize(
    createConnection(connections.get(process.env.NODE_ENV)),
    [
      new UsersController(),
    ]
  );

  if (!isUnitTests) {
    app.listen();
  }

  return app;
}

export default init();