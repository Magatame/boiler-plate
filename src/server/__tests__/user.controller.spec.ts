import server from "../server";
import request from 'supertest';
import express from 'express';
import { getConnection } from "typeorm";
import { User } from "../entity/User";


let app = express();
beforeAll(async () => {
  await server.then(server => app = server.app);
});

afterAll(async () => {
  let conn = getConnection();
  if (conn.isConnected) {
    return conn.close();
  }
});


it('should allow users to register', async () => {
    // Arrange
    const user = new User();
    user.age = 42;
    user.lastName = 'Smith';
    user.firstName = 'John';
    
    return request(app).post('/api/users')
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.firstName).toEqual(user.firstName)
        expect(response.body.lastName).toEqual(user.lastName)
        expect(response.body.age).toEqual(user.age)
        expect(response.body.id).toBeGreaterThan(0)
    });
});