import express from 'express';
import * as bodyParser from 'body-parser';
import {Connection} from "typeorm";
import IController from './controllers/baseController.interface';
 
class App {
  public app = express();
  public port: number = 8080;
  public ready: boolean = false;

              
  public async initialize(connection : Promise<Connection>, controllers : [IController]) {
    await connection.then(_ => {
      this.initializeMiddlewares();
      this.initializeControllers(controllers);
    });
  }
 
  private initializeMiddlewares() {
    this.app.use(bodyParser.json());
  }
 
  private initializeControllers(controllers : [IController]) {
    controllers.forEach((controller) => {
      this.app.use('/api/', controller.router);
    });
  }
 
  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);      
    });
  }
}
 
export default App;