import * as React from "react";
import '../index.css';
import { Component } from "react";
import * as ReactImage from "../public/react.png"

export default class App extends Component {
  state = { firstName : '' as string };

  componentDidMount() {
    fetch('/api/users')
      .then(res => res.json())
      .then(user => this.setState({ firstName: user.firstName }));
  }

  render() {
    const { firstName } = this.state;
    return (
      <div>
        {firstName ? <h1>{`Hello ${firstName}`}</h1> : <h1>Loading.. please wait!</h1>}
        <img src={ReactImage} alt="react" />
      </div>
    );
  }
}
