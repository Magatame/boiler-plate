// VS Code will complain about image imports without the line below
/// <reference path="../global.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App';

ReactDOM.render(<App />, document.getElementById('root'));
