#!/bin/bash

echo "Waiting for DB"
./.devcontainer/wait-for-it.sh -t 30 db:3306 -- echo "DB READY"

echo "Executing NPM"
exec npm run dev